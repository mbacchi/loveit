User-agent: *

Disallow: /images/
Disallow: /js/
Disallow: /css/

User-agent: MJ12bot
Disallow: /

User-agent: AhrefsBot
Disallow: /

User-agent: BLEXBot
Disallow: /

# Block SISTRIX
User-agent: SISTRIX Crawler
Disallow: /
User-agent: sistrix
Disallow: /
User-agent: 007ac9
Disallow: /
User-agent: 007ac9 Crawler
Disallow: /

# Block Uptime robot
User-agent: UptimeRobot/2.0
Disallow: /

# Block Ezooms Robot
User-agent: Ezooms Robot
Disallow: /

# Block Perl LWP
User-agent: Perl LWP
Disallow: /

# Block netEstate NE Crawler (+http://www.website-datenbank.de/)
User-agent: netEstate NE Crawler (+http://www.website-datenbank.de/)
Disallow: /

# Block WiseGuys Robot
User-agent: WiseGuys Robot
Disallow: /

# Block Turnitin Robot
User-agent: Turnitin Robot
Disallow: /

# Block Heritrix
User-agent: Heritrix
Disallow: /

# Block pricepi
User-agent: pimonster
Disallow: /

User-agent: SurdotlyBot
Disallow: /

User-agent: ZoominfoBot
Disallow: /

Sitemap: {{ "/sitemap.xml" | absLangURL }}

## Borrowed AI blocks from:
# 1. https://netfuture.ch/2023/07/blocking-ai-crawlers-robots-txt-chatgpt/
# 2. https://darkvisitors.com/

# Used for many other (non-commercial) purposes as well
User-agent: CCBot
Disallow: /

# For new training only
User-agent: GPTBot
Disallow: /

# Not for training, only for user requests
User-agent: ChatGPT-User
Disallow: /

# Marker for disabling Bard and Vertex AI
User-agent: Google-Extended
Disallow: /

User-agent: FacebookBot
Disallow: /

# Multi-purpose, commercial uses; including LLMs
User-agent: Omgilibot
Disallow: /

# This blocks anthropic-ai
User-agent: anthropic-ai
Disallow: /
